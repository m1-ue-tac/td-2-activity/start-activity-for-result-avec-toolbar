package com.univlille.startactivityforresult;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.ShareActionProvider;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.MenuItemCompat;

// On a ajouté la barre de tâches en haut pour faire "joli"

public class MainActivity extends AppCompatActivity {

    TextView textView1;
    Button button1;

    public static final int CODE_SECONDE_ACTIVITE = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // ajout de la toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // La toolbar est maintenant définie comme barre d'action, donc on peut par exemple faire :
        // ActionBar actionBar=getSupportActionBar();
        // puis
        // actionBar.hide();
        // actionBar.show();


        textView1 = (TextView) findViewById(R.id.textView1);
        button1 = (Button) findViewById(R.id.button1);

        button1.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                intent.putExtra("message", textView1.getText());
                startActivityForResult(intent, CODE_SECONDE_ACTIVITE);
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        /* Maintenant on vérifie :
           - que c'est bien la seconde activité qui est finie
           - si la seconde activité s'est terminée bien ou mal
           - si les data sont non null
           - si la data attendue est présente
          */

        switch (requestCode) {
            case CODE_SECONDE_ACTIVITE:
                switch (resultCode) {
                    case RESULT_OK:
                        Toast.makeText(this, "Validé", Toast.LENGTH_SHORT).show();
                        if (data != null) {
                            // Dans l'absolu, on devrait aussi tester l'existence de la clé "MESSAGE" ! (ou try-catch...)
                            String message = data.getStringExtra("MESSAGE");
                            if (message == null) {
                                Toast.makeText(this, "pb", Toast.LENGTH_SHORT).show();
                            }
                            textView1.setText(message);
                        } else Toast.makeText(this, "pas de data", Toast.LENGTH_SHORT).show();
                        return;
                    case RESULT_CANCELED:
                        Toast.makeText(this, "Refusé", Toast.LENGTH_SHORT).show();
                        return;
                    default:
                        throw new IllegalStateException("Unexpected value: " + resultCode);
                }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu, menu);

        MenuItem.OnActionExpandListener expandListener = new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem menuItem) {
                message("clic sur bouton de recherche");
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem menuItem) {
                message("clic sur bouton 'up' pour sortir de la recherche");
                return true;
            }
        };
        MenuItem searchItem = menu.findItem(R.id.action_search);
        searchItem.setOnActionExpandListener(expandListener);
        SearchView searchView =
                (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                message("va rechercher : " + query);
                // ici, il y a encore du travail à faire pour fermer la recherche éventuellement,
                // supprimer le texte saisi dans la zone de recherche, etc...
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                message("recherche basée sur :" + newText);
                return false;
            }
        });

//        // exemple basique pour le "partage"
//        MenuItem shareItem = menu.findItem(R.id.action_share);
//        ShareActionProvider myShareActionProvider =
//                (ShareActionProvider) MenuItemCompat.getActionProvider(shareItem);
//
//        Intent myShareIntent = new Intent(Intent.ACTION_SEND);
//        myShareIntent.setType("image/*");
//        myShareIntent.putExtra(Intent.EXTRA_STREAM, "https://images.prismic.io/the-ringio/48ce7886-cdf5-4cd4-970e-60cb54797d1b_lille_grand_place_ville_fontaine_2.jpg?auto=format&rect=0%2C1140%2C3648%2C3648&w=320&h=320&fit=crop&q=75");
//        myShareActionProvider.setShareIntent(myShareIntent);

        return true;
    }

    private void message(String str) {
        Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_favorite:
                Toast.makeText(this, "Favoris !", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.action_settings:
                Toast.makeText(this, "Sous-menu !", Toast.LENGTH_SHORT).show();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
