package com.univlille.startactivityforresult;

import static android.app.Activity.RESULT_OK;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class SecondActivity extends AppCompatActivity {
    EditText editText1;
    Button button1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        editText1 = (EditText) findViewById(R.id.editText1);
        button1 = (Button) findViewById(R.id.button1);

        // ajout de la toolbar
        Toolbar toolbar = findViewById(R.id.toolbar2);
        setSupportActionBar(toolbar);
        ActionBar actionBar=getSupportActionBar();
        // on active le bouton 'up' qui est connecté à MainActivity grâce au Manifest.
        // !!!! ATTENTION ! Cela vous ramènera sur l'activité 1 sans passer
        //  par le onActivityResult !!!!! cela revient à faire un UNDO sans gestion associée.
        actionBar.setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        editText1.setText(intent.getStringExtra("message"));

        button1.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View view) {
                String message = editText1.getText().toString();
                Intent intent = new Intent();
                intent.putExtra("MESSAGE", message);
                setResult(RESULT_OK, intent);

                finish();
            }
        });
    }

}
